import { Component, OnInit } from '@angular/core';
import { Destination } from '../models/destination';

@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent implements OnInit {

  readonly IMG_DEFAULT_PATH = '../../assets/images/paolo-nicolello-2gOxKj594nM-unsplash.jpg';

  destinations: Destination[];

  constructor() { }

  ngOnInit(): void {
    this.destinations = [
      new Destination('Bogota', this.IMG_DEFAULT_PATH),
      new Destination('Lima', this.IMG_DEFAULT_PATH)
    ];
  }

  addDestination(name: String, imageUrl: String): boolean {
    if(!name && !imageUrl) {
      return;
    }
    const destination = new Destination(name, imageUrl);
    this.destinations.push(destination);
    return false;
  }

}
