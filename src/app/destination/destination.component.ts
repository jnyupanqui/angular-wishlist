import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Destination } from '../models/destination';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.css']
})
export class DestinationComponent implements OnInit {

  @Input()
  destination: Destination;

  @HostBinding('style')
  widthFull = 'width: 100%;display:inline-block;';

  constructor() { }

  ngOnInit(): void {
  }

}
