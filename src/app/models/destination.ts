export class Destination {
    name: String;
    imageUrl: String;
    
    constructor(name: String, imageUrl: String) {
        this.name = name;
        this.imageUrl = imageUrl;
    }
}